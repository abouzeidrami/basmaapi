# Git repo

```bash
git clone https://rabouzeid@bitbucket.org/abouzeidrami/basmaapi.git
```

## Database
Create a database called laravel and import to it "laravel.sql"

## Laravel installation
Make sure to fix the credentials in ".env" and set the db connection user and password:
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```
Follow the below steps to run laravel:
```bash
composer install
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
php artisan jwt:secret
php artisan serve
```
Go to: [http://127.0.0.1:3000/](http://127.0.0.1:3000/)
## Postman
Collection: "Basma Api.postman_collection.json"
[Documentation](https://documenter.getpostman.com/view/1822876/TzCLA9dz)

## Credentials
Superadmin: rabouzeid87@gmail.com/test123