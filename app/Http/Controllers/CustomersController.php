<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Validator;


class CustomersController extends Controller
{
    /**
     * Create a new CustomersController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api');
    }

    /**
     * Get the users list array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function list(){
        if(auth()->user()->role !== 1){
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $users = DB::table('users')->get();
        return response()->json([
            'users' => $users
        ]);
    }

    /**
     * Get the users list array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function average(Request $request){
        if(auth()->user()->role !== 1){
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $validator = Validator::make($request->all(), [
            'start_date' => 'required|date',
            'end_date' => 'required|date'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $start_date = $request->start_date;
        $end_date = Carbon::parse($request->end_date)->addDays(1);
        //$users = DB::table('users')->whereBetween('created_at', [$start_date, $end_date])->get();

        $count = User::selectRaw('DATE(created_at) as date, COUNT(*) as count')
        ->groupBy('date')
        ->where('created_at', '>', Carbon::now()->subWeek())
        ->whereBetween('created_at',  [$start_date, $end_date])
        ->get();
    

        
        return response()->json([
            'count' => $count
        ]);
    }

}